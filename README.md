#User List UI

I've pulled this architecture from a larger application I developed and refactored it to manage a simple user list.
For more information on that you can see the "disclaimer" at the bottom.
The application itself allows you to register a user, login, view a list of users and select/delete users.

There is a known flaw where you can delete your own user and remain logged in! I'll will make modifications to this as I have more time.

A working example can be seen here: http://expl.reifynow.com/

A new user may be created (registered) or if exists you can login with (testuser/password)

This document explains how to setup the user list ui sample application, debug in browser, and run basic commands.

##Prerequisites

- The user-list-api node service running and pointed to. You can update the properties.json file respectively for this.
- Install **NodeJS v7.x.x** or greater and **npm v 4.x.x** or greater.
- Install **gulp-cli** globally: `npm install gulp-cli -g`

----

###Setup the application environment
  Clone the GIT repository for User List UI: `git clone https://bitbucket.org/reifynow/user-list-ui.git`
  
  Install node dependencies:
  
    # Navigate into the project directory on your dev machine.
    cd user-list-ui
    
    # Install project dependencies via npm.
    npm install
    
###Debugging

  To debug the application in the browser you can use the `npm start` command.
  This will compile the typescript files, start the lite server with file watching, and launch the browser pointing to the lite server.
  Any file changes will automatically be refreshed in the browser to enable live code editing.
  
  To only compile the typescript run the typescript compiler: `npm run tsc`
  
  By using either `npm start` or `npm run tsc`, when the typescript compiler runs it will error if any issues are found within the project code giving detailed explanation with file and line/char number.
  
###Gulp Tasks

  These tasks will be updated as development and setup continues.

  **Bundle assets**
  `gulp bundle`
  
  **Clean the build assets directory**
  `gulp clean-bundle`

  **Clean the build**
  `gulp clean-build`
  
  **Clean ahead of time compiled files**
  `gulp clean-aot`
  
  **Clean all**
  `gulp clean`
----
##NPM Script Commands

**The following outlines useful commands per the initial angular4 application setup.**

Install the npm packages described in the `package.json` and verify that it works:

```bash
npm install
npm start
```

The `npm start` command first compiles the application, 
then simultaneously re-compiles and runs the `lite-server`.
Both the compiler and the server watch for file changes.

Shut it down manually with Ctrl-C.

### npm scripts

We've captured many of the most useful commands in npm scripts defined in the `package.json`:

* `npm start` - runs the compiler and a server at the same time, both in "watch mode".
* `npm build` - runs the TypeScript compiler once.
* `npm build:aot` - compiles using the aot compiler and tree shakes and bundles using rollup.
* `npm build:prod` - cleans build location, runs build:aot plus copies the distribution files and bundles assets into the build location 
* `npm serve:build` - launch lite-server pointed towards the build directory.

### Unit Tests
Comming

### End-to-end (E2E) Tests

Comming

### Disclaimer
This application isn't perfect for UX flow, and has some UX flaws, as so it also needs some clean up and a little re organization as it has been pulled out of a larger project, however for the best of it it lays out an Angular 4 architecture for handling authentication, session, and many necessary core features for most real world applications.