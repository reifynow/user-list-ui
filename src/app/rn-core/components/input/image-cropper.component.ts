import {Component, Output, EventEmitter, ViewChild, OnInit, AfterViewInit} from '@angular/core';
import {CropperSettings, ImageCropperComponent} from '../image-cropper/index';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

declare var jQuery : any;

@Component({
  selector: 'ngbd-modal-content',
  template: `
  <div class="container-fluid">
      <div class="title">
        <h4>Upload Image</h4>
      </div>
      <div class="row">
      <div class="col-md-12" style="">
      <div class="cropperWrapper" style="width:420px; padding-bottom:0; padding-left: 10px;">
      <img-cropper #cropper [image]="data" [settings]="cropperSettings"></img-cropper><br>
      </div>
      </div>
      <div class="col-md-12" style="margin-top: 8px; margin-bottom: 8px; text-align: center;">
        <label for="cropperInput">Select Image</label>
        <input type="file" style="display:none" id="cropperInput" (change)="fileChangeListener($event)" />
      </div>

      <div class="col-md-12" style="text-align: right; margin-top: 5px; margin-bottom: 10px;">
      <button (click)="close()" class="profile-button"><span class="prof-butt-title">Cancel</span></button>
      <button (click)="save()" class="profile-button"><span class="prof-butt-title">Save Image</span></button>
  </div>
  </div>
  </div>
  `,
  styles : [`
    .title {
      border-bottom: 1px solid #ccc;
    }
    .cropperWrapper {
      width: 400px;
      margin: 10px auto 0px;
    }
    label {
      background-color: #FFF;
      border: 1px solid #D9D9D9;
      margin: 0 5px;
      font-size: 17px;
      border-radius: 6px;
      min-width: 40px;
      padding: 0 12px;
      height: 34px;
      cursor: pointer;
      -webkit-box-shadow: 0px 2px 3px -1px #c9c7c9;
      -moz-box-shadow: 0px 2px 3px -1px #c9c7c9;
      box-shadow: 0px 2px 3px -1px #c9c7c9;
      color: #595959;
      font-size: 13px;
      line-height: 32px;
    }
    label:hover {
      opacity: .8;
    }
  `]
})
export class ImageCropperModal {
  public data : any;
  cropperSettings: CropperSettings;
  currentFile : any;
  @ViewChild('cropper', undefined) cropper : ImageCropperComponent;

  constructor(public activeModal: NgbActiveModal) {

    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 200;
    this.cropperSettings.height = 200;

    this.cropperSettings.croppedWidth = 200;
    this.cropperSettings.croppedHeight = 200;

    this.cropperSettings.canvasWidth = 400;
    this.cropperSettings.canvasHeight = 300;
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.minWidth = 50;
    this.cropperSettings.minHeight = 50;
    this.cropperSettings.keepAspect = true;
    this.cropperSettings.rounded = false;

    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,255,255, 0.5)';
    this.cropperSettings.cropperDrawSettings.strokeWidth = 1;

    this.data = {};
  }

  fileChangeListener($event) {
    var image:any = new Image();
    var file:File = $event.target.files[0];
    this.currentFile = $event.target.files[0];
    var myReader:FileReader = new FileReader();
    myReader.onloadend = (loadEvent:any) => {
      image.src = loadEvent.target.result;
      this.cropper.setImage(image);
    };
    myReader.readAsDataURL(file);
  }

  dataURItoBlob(dataURI:any) {
    if (!dataURI) return null;
    let byteString : string = '';
    if (dataURI.split('.')[0].indexOf('base64') >= 0)
      byteString = atob(dataURI.split(',')[1]);
    else
      byteString = decodeURIComponent(dataURI.split(',')[1]);
    let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    let ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], {type:mimeString});
  }


  save () {
    if (!this.currentFile) {
      this.data.image = "";
    }
    let blob = this.dataURItoBlob(this.data.image);
    if (blob === null) this.activeModal.dismiss('Invalid image file.');
    else this.activeModal.close({data: this.data, blob: this.dataURItoBlob(this.data.image), fileName: this.currentFile.name});
  }

  close() {
    this.activeModal.dismiss();
  }
}
