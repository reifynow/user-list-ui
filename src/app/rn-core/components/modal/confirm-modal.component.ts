import { Component, Input } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

export enum ModalResult {
  Ok,
  Cancel
}

@Component({
  selector: 'ngb-modal-content',
  template: `
    <div class="modal-header">
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
      <h4 class="modal-title">{{title}}</h4>
    </div>
    <div class="modal-body" [innerHTML]="content">
    </div>
    <div *ngIf="buttons" class="modal-footer">
        <button *ngFor="let button of buttons" type="button" class="btn btn-secondary" (click)="activeModal.close(button.toLowerCase())">{{button}}</button>
    </div>
    <div *ngIf="!buttons" class="modal-footer">
        <button type="button" class="btn btn-secondary" (click)="closeModal('Ok')">Ok</button>
      <button type="button" class="btn btn-secondary" (click)="closeModal('Cancel')">Cancel</button>
    </div>
  `
})
export class ConfirmModalComponent {
  @Input() title;
  @Input() content;
  @Input() buttons;

  constructor(public activeModal: NgbActiveModal) { }

  closeModal(result): void {
    this.activeModal.close(ModalResult[result]);
  }
}
