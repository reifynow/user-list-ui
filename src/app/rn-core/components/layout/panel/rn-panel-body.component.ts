import {Component} from '@angular/core';

@Component({
  selector: 'rn-panel-body',
  template: `
    <div class="rn-panel-body"><ng-content></ng-content></div>
  `,
  styleUrls: ['./rn-panel.css']
})
export class RnPanelBody {
  constructor() {};
}
