import {Component, Input} from '@angular/core';

@Component({
  selector: 'rn-panel-header',
  template: `
    <div class="rn-panel-head" [class.small]="size === 'small'"><h3>{{title}}</h3><ng-content></ng-content></div>
  `,
  styleUrls: ['./rn-panel.css']
})
export class RnPanelHeader {
  @Input() title : string;
  @Input() size : string = "";
  constructor() {}
}
