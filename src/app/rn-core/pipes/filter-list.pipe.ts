import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'filterListPipe' })
export class FilterListPipe implements PipeTransform {
  transform(list: any, val : string) {
    return list.filter(item => {
      val = val.trim().toLowerCase();
      return (~item.FirstName.toLowerCase().indexOf(val)
      || ~item.LastName.toLowerCase().indexOf(val)
      || ~(item.FirstName.toLowerCase()+' '+item.LastName.toLowerCase()).indexOf(val)
      || ~item.Email.toLowerCase().indexOf(val) || val == '');
    });
  }
}
