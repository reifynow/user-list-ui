import {Pipe} from '@angular/core';

@Pipe({
  name: 'phone'
})
export class RnPhonePipe {
  transform(val, args) {
    if (!val) return '';
    let num = val.match(/\d+/g);
    if (!num) return '';
    num = num.join('');
    if (num.length<7) return num;
    let phoneArr = [];
    let retVal = '';
    for (let i = 0; i < 4; i++)
      if (num.substr(i*3, 3).length) phoneArr.push(num.substr(i*3, 3));
    if (phoneArr.length > 3) retVal = '('+phoneArr.splice(0, 1).join()+') ';
    retVal += phoneArr.splice(0, 2).join('-');
    return retVal+phoneArr[0];
  }
}
