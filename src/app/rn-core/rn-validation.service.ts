import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {validateEqual} from './equal.validator';

export class FormElement {
  name : string;
  prettyName : string;
  value: any;
  validations : Array<string>;
}

export class RnValidationService {
  customGroup : any = {}; //{name : {value: '', validators: []}
  formElements : Array<FormElement> = [];

  private validationTypes : any = {
    required: {
      name : 'required',
      message : 'is required.',
      validator : Validators.required
    },
    email : {
      name : 'pattern',
      message : 'Must use a valid email address.',
      validator : Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')
    },
    validateEqual : {
      name : 'validateEqual',
      message: 'Passwords must match.'
    }
  };

  formErrors : any = {}; /*{
    [key : string] : {
      message: string;
      cssClass : string;
    }
  } = {};*/

  validationMessages : any = {}; /*{
    [key : string] : any
  } = {};*/

  constructor(private formGroup : FormGroup, private formBuilder : FormBuilder) {
  }

  addMatchGroup(name : string, value: any, matchName : string, reverse : boolean, validators : Array<string>) {
    let validatorsArr = [
      validateEqual(matchName, reverse)
    ];
     validators.forEach(validator => {
      validatorsArr.push(this.validationTypes[validator].validator)
    });
    this.customGroup[name] = {value: value, validators: validatorsArr};
    this.loadElements([{name: name, prettyName: '', value: value, validations: ['required', 'validateEqual']}]);
  }

  private loadElements(formElements : Array<FormElement>) {
    this.formElements = formElements;
    formElements.forEach(element => {

      this.formErrors[element.name]={
        message: '', cssClass: ''
      };

      this.validationMessages[element.name] = {};
      element.validations.forEach(validation => {
        let message = '';
        switch(validation) {
          case 'required':
            message = element.prettyName + this.validationTypes[validation].message;
            break;
          case 'email':
            message = this.validationTypes[validation].message;
                break;
          case 'validateEqual':
            message = this.validationTypes[validation].message;
                break;
        }
        this.validationMessages[element.name][this.validationTypes[validation].name] = message;
      });
    });
  }

  updateValues(values: any) {
    this.formGroup.setValue(values);
  }

  loadFormObjects(formElements : Array<FormElement>) : FormGroup {
    this.loadElements(formElements);
    this.buildForm();
    return this.formGroup;
  }

  buildForm() : void {
    let group = {};
    this.formElements.forEach((fel: FormElement) => {
      let validators = [];
      fel.validations.forEach((v : string) => {
        validators.push(this.validationTypes[v].validator)
      })
      group[fel.name] = [fel.value, validators]
    });
    for (let key in this.customGroup) {
      group[key] = [this.customGroup[key].value, this.customGroup[key].validators];
    }

    this.formGroup = this.formBuilder.group(group);

    this.formGroup.valueChanges.subscribe(data => this.validateForm(data));
    this.validateForm();
  }

  validateForm(data ?: any) {
    this.doValidateForm();
  }

  validateCleanForm(displayValid : boolean = false) {
    this.doValidateForm(true, displayValid);
  }

  doValidateForm(clean : boolean = false, showCleanValid : boolean = false) {
    if (!this.formGroup) {return; }
    const form = this.formGroup;

    for (const field in this.formErrors) {
      const control = form.get(field);
      if (control && (control.dirty || control.touched) && !control.pristine) this.formErrors[field] = {message: '', cssClass: 'input-valid'};
//      else this.formErrors[field] = {message: '', cssClass: ''};
      if (control && clean && (control.dirty && !control.touched) && control.valid) this.formErrors[field] = {message: '', cssClass: ''};
      if (control && clean && showCleanValid && control.valid) this.formErrors[field] = {message: '', cssClass: 'input-valid'};
      if (control && (clean || (control.dirty || control.touched && !control.pristine)) && !control.valid) {
        const messages = this.validationMessages[field];
        this.formErrors[field].message = '';
        for (const key in control.errors) {
          this.formErrors[field].message += messages[key] + ' ';
          let cssClass : string = null;
          switch(key) {
            case 'required':
              cssClass = 'has-error';
              break;
            case 'pattern':
              cssClass = 'has-error';
              break;
            case 'validateEqual':
              cssClass = 'has-error';
              break;
          }
          if (cssClass) this.formErrors[field].cssClass = cssClass
        }
      }
    }

  }

  setInvalid(controlName : string) {
    this.formErrors[controlName].cssClass='input-error';
  }

}
