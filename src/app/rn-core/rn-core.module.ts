import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RnComponentsModule } from './components/rn-components.module';
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { RnPhonePipe } from './pipes/phone-number.pipe';
import { EqualValidator } from './equal.validator';
import { ConfirmModalComponent } from './components/modal/confirm-modal.component';
import { ScrollAnchorComponent } from './directives/scroll-to-anchor';
import { FilterListPipe } from './pipes/filter-list.pipe';
import { PropertyPipe } from './pipes/property.pipe';

@NgModule({
  imports: [
    CommonModule,
    RnComponentsModule
  ],
  declarations: [
    ClickOutsideDirective,
    RnPhonePipe,
    EqualValidator,
    ConfirmModalComponent,
    ScrollAnchorComponent,
    FilterListPipe,
    PropertyPipe
  ],
  providers: [
  ],
  entryComponents: [
    ConfirmModalComponent
  ],
  exports: [
    RnComponentsModule,
    ClickOutsideDirective,
    EqualValidator,
    ConfirmModalComponent,
    ScrollAnchorComponent,
    FilterListPipe,
    PropertyPipe
  ]
})
export class RnCoreModule {
};
