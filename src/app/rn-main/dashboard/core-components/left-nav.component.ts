import {Component, Input} from '@angular/core';

@Component({
  selector: 'left-nav',
  template: `
<nav id="left-panel" class="left-panel">

  <!--mobile -->
  <ul class="left-panel-list mobile-list">
    <li><a href=""><span class="left-menu-icon"><i class="zmdi zmdi-account"></i> </span><span class="left-menu-title">{{userProfile.firstName}} {{userProfile.lastName}}</span></a></li>
    <li><a href=""><span class="left-menu-icon"><i class="zmdi zmdi-search"></i></span><span class="left-menu-title">Search</span></a></li>
    <li><a href=""><span class="left-menu-icon"><i class="zmdi zmdi-apps"></i></span><span class="left-menu-title">Applications</span></a></li>
    <li><a href=""><span class="left-menu-icon"><i class="fa fa-envelope" aria-hidden="true"></i></span><span class="left-menu-title">Messages</span></a></li>
    <li><a href=""><span class="left-menu-icon"><i class="fa fa-tasks" aria-hidden="true"></i></span><span class="left-menu-title">Tasks</span></a></li>
    <li><a href=""><span class="left-menu-icon"><i class="fa fa-bell" aria-hidden="true"></i></span><span class="left-menu-title">Notifications</span></a></li>
  </ul>

  <div class="mobile-menu-title">Main</div>
  <!--  -->

  <ul class="left-panel-list">
  
  
  <ng-content></ng-content>
  
  
    </ul>

  <!--mobile-->
  <ul class="left-panel-list mobile-list mobile-list-margin">
    <li><a href=""><span class="left-menu-icon"><i class="zmdi zmdi-settings"></i></span><span class="left-menu-title">Settings</span></a></li>
    <li><a href=""><span class="left-menu-icon"><i class="fa fa-sign-out" aria-hidden="true"></i></span><span class="left-menu-title">Logout</span></a></li>
  </ul>
  <!---->
</nav>

  `
})
export class LeftNavComponent {
  @Input('user') userProfile : any = {};
}
