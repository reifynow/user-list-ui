import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TopNavComponent} from './top-nav.component';
import {LeftNavComponent} from './left-nav.component';
import {PageHeaderComponent} from "./page-header.component";
import {PageTabsComponent} from "./page-tabs.component";
@NgModule({
  imports : [
    CommonModule,
    RouterModule
  ],
  declarations: [
    TopNavComponent,
    LeftNavComponent,
    PageHeaderComponent,
    PageTabsComponent
  ],
  exports : [
    TopNavComponent,
    LeftNavComponent,
    PageHeaderComponent,
    PageTabsComponent
  ]
})
export class CoreComponentsModule {}
