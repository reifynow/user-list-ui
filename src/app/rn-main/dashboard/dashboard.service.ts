import {Injectable} from '@angular/core';
import {HttpClient} from '../../rn-secure/http-client';
import {SecureDataService} from '../../rn-secure/secure-data.service';
import {Observable} from 'rxjs/Rx';
@Injectable()
export class DashboardService {
    private apiUrl: string;
    constructor(private http: HttpClient, secureDataService: SecureDataService) {
        this.apiUrl = secureDataService.getApiUrl();
    }

    getUsers() : Observable<any> {
        return this.http.get(this.apiUrl + '/').map(res => res.json());
    }

    deleteUser(id : string) : Observable<any> {
        return this.http.delete(this.apiUrl + '/'+id).map(res => res.json());
    }

    updateUser(id : string, user : any) : Observable<any> {
        return this.http.put(this.apiUrl + '/'+id, user).map(res => res.json());
    }
}