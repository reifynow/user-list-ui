import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {RnCommonModule} from '../../rn-common/rn-common.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
import {CoreComponentsModule} from './core-components/core-components.module';
import {DashboardComponent} from './dashboard.component';
import {CheckboxModule} from 'primeng/primeng';
import {RnCoreModule} from "../../rn-core/rn-core.module";
import {HomeComponent} from "./manage/home.component";
import {UsersComponent} from "./manage/users.component";
import {DashboardService} from "./dashboard.service";

@NgModule({
  imports : [
    CommonModule,
    BrowserAnimationsModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    RnCommonModule,
    RnCoreModule,
    CoreComponentsModule,
      CheckboxModule
  ],
  declarations: [
    DashboardComponent,
      HomeComponent,
      UsersComponent
  ],
  entryComponents: [
  ],
  providers: [
      DashboardService
  ]
})
export class DashboardModule {}
