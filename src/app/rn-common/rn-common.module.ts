import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbComponent} from './components/breadcrumb.component';
import { NavbarComponent} from './components/navbar.component';
import { SearchComponent } from './components/search.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    BreadcrumbComponent,
    NavbarComponent,
    SearchComponent
  ],
  exports: [
    BreadcrumbComponent,
    NavbarComponent,
    SearchComponent
  ]
})
export class RnCommonModule {
};
