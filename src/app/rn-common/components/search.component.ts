import {Component} from '@angular/core';

@Component({
  selector: 'search',
  template: `
      <span class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></span>
      <input class="input-search" type="text" placeholder="TYPE HERE TO SEARCH">
  `,
  styles: [`
    .search-icon {
      color: #fff;
    }
    
    .input-search {
      border: none;
      background-color: transparent;
      padding: 0 10px;
      color: #fff;
    }

  `]
})
export class SearchComponent {}

