import {NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {HttpClient} from './http-client';
import {SecureDataService } from './secure-data.service';
import {AuthGuard } from './auth-guard.service';
import {AuthService } from './auth.service';

@NgModule({
  imports:  [
    CommonModule,
    HttpModule,
    RouterModule
  ],
  providers : [
    SecureDataService,
    AuthGuard,
    HttpClient,
    AuthService
  ]
})
export class SecureModule{};
