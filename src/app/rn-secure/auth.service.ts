import {Injectable} from '@angular/core';
import {ReplaySubject} from 'rxjs/Rx';

export class AuthData {
  accessToken : string = null;
  expires : number = 3600000;
  accessType : string = 'Bearer';

  constructor() {}
}

@Injectable()
export class AuthService {
  user : any = null;
  authData : AuthData = new AuthData();
  sessionData : any = {};
  onTimeout : any = null;
  public getUser : ReplaySubject<any> = new ReplaySubject(1);

  private expiresTimeout : any = null;

  constructor() {
    this.loadCache();
  }

  public setLoggedIn(responseAuthData : any, user : any) {
    if (responseAuthData && responseAuthData.accessToken) {
      this.authData.accessToken = responseAuthData.accessToken;
      this.authData.expires = responseAuthData.expires > 86400 ? 86400 : responseAuthData.expires;
      this.authData.accessType = responseAuthData.accessType;
      this.user = user;
      this.saveSession();

      this.setExpiresTimeout();
    }
  }

  public isLoggedIn() : boolean {
    if (this.authData.accessToken) return true;
    return false;
  }

  public setLoggedOut() {
    this.authData = new AuthData();
    this.user = null;
    this.clearSession();
    if (this.expiresTimeout) clearTimeout(this.expiresTimeout);
  }

  public setUser(user : any) {
    this.user = user;
    this.getUser.next(user);
  }

  public setSessionItem(key : string, item : any) {
    this.sessionData[key] = item;
  }

  public refreshSession() {
    if (this.isLoggedIn()) {
      this.setExpiresTimeout();
    }

  }

  private setExpiresTimeout() {
    this.expiresTimeout = setTimeout(this.timeOutSession.bind(this), this.authData.expires*1000);
  }

  private timeOutSession() {
    if (this.onTimeout && typeof(this.onTimeout) == 'function') this.onTimeout(this.setLoggedOut.bind(this));
    else this.setLoggedOut();
  }

  public saveSession() {
    window.localStorage.setItem('user', JSON.stringify(this.user));
    window.localStorage.setItem('authData', JSON.stringify(this.authData));
    window.localStorage.setItem('sessionData', JSON.stringify(this.sessionData));
  }

  private clearSession() {
    this.sessionData = {};
    window.localStorage.removeItem('user');
    window.localStorage.removeItem('authData');
    window.localStorage.removeItem('sessionData');
    this.onTimeout = null;
  }

  loadCache() {
    let user = window.localStorage.getItem('user') || null;
    let authData = window.localStorage.getItem('authData') || null;
    let sessionData = window.localStorage.getItem('sessionData') || null;
    if (user !== null && user !== undefined) this.user = JSON.parse(user);
    this.getUser.next(this.user);
    if (authData !== null && authData !== undefined) this.authData = JSON.parse(authData)
    if (sessionData !== null && sessionData !== undefined) this.sessionData = JSON.parse(sessionData)
  }

}
