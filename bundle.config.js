module.exports = {
  bundle: {
    main: {
      scripts: [
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/bootstrap/dist/js/bootstrap.min.js',
        './node_modules/core-js/client/shim.min.js',
        './node_modules/zone.js/dist/zone.js'
      ],
      styles: [
        './src/assets/styles/icon_font/css/font-awesome.css',
        './node_modules/bootstrap/dist/css/bootstrap.min.css',
        './node_modules/primeng/resources/themes/omega/theme.css',
        './node_modules/primeng/resources/primeng.min.css',
        './src/assets/styles/global_overrides.css'
      ],
      options: {
        rev: false
      }
    }
  }//,
//  copy: './src/assets/**/*.{png,svg,jpg,gif}'
};
