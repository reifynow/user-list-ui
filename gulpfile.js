var gulp = require('gulp');
var exec = require('child_process').exec;
var rename = require('gulp-rename');
var runSequence = require('run-sequence');
var util = require('gulp-util');
var bundle = require('gulp-bundle-assets');
var del = require('del');
var q = require('q');


gulp.task('default', function() {
  // place code for your default task here
});

gulp.task('bundle', function() {
  gulp.src([
    './src/assets/fonts/**/*.*',
    './src/assets/styles/icon_font/fonts/**/*.*'
  ])
    .pipe(gulp.dest('./build/public/fonts'));

  gulp.src('./src/assets/images/**/*.*')
    .pipe(gulp.dest('./build/public/images'));

  return gulp.src('./bundle.config.js')
    .pipe(bundle())
    .pipe(gulp.dest('./build/public/assets'));
});

gulp.task('clean-bundles', function() {
  return del([
    './build/public'
  ]);
});

gulp.task('clean-aot', function() {
  return del(['./aot']);
});

gulp.task('clean-build', function() {
  return del(['./build']);
});

gulp.task('clean', function(cb) {
  runSequence('clean-aot', 'clean-build', cb);
});
