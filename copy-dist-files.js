var fs = require('fs');
var resources = [
  'src/properties.json',
  'aot-res/index.html',
  'src/countries.json'
];
resources.map(function(f) {
  var path = f.split('/');
  var t = 'build/' + path[path.length-1];
  fs.createReadStream(f).pipe(fs.createWriteStream(t));
});
